# Automatic Send Emails

_An application developed in Java to create and implement a queue and a lambda that is triggered by an event in that queue_

## What it does ...

_Trigger from SQS queue to send email_

## To consider ...

Since the application uses the event generated in a queue as the trigger for the lambda AutomaticSendEmail, make sure that the event is generated with the necessary data that will be described below: **fromName**, name of the sender; **to**, recipient's email; **subject**, subject and **emailBody** which will be the body of the email. On the other hand, the following environment variables must be established: **SMTP_USERNAME**, Amazon SES SMTP username; **SMTP_PASSWORD**, password for Amazon SES SMTP; **CONFIGSET** (Optional), the name of a configuration set which must be used in conjunction with the "X-SES-CONFIGURATION-SET" header; **HOST** is defined for default as email-smtp.us-west-2.amazonaws.com, but If you're using Amazon SES in a region other than EE.UU. Oeste (Oregón), replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP endpoint in the appropriate AWS Region; PORT, The port to which you will connect on the Amazon SES SMTP endpoint. **PORT**, 587 has been chosen because STARTTLS will be used to encrypt the connection, but it can also be 465, 2465, 2587 ...; **FROM**, source email.

### How to use it?
### Example

_From the lambda console:_

```java
{
  "Records": [
    {
      "body": {
        "email": {
          "to": "toone@toone.com",
          "fromName": "Remitent Name One",
          "subject": "Subject One",
          "emailBody": " ... "
        }
      }
    },
    {
      "body": {
        "email": {
          "to": "totwo@totwo.como",
          "fromName": "Remitent Name Two",
          "subject": "Subject Two",
          "emailBody": " ... "
        }
      }
    }
  ]
}
```
_From api or a client REST:_

```java
{
  "email": {
    "fromName":"Remitent Name",
    "to":"to@to.com",
    "subject":"Subject",
    "emailBody":"....."
  }
}
```

## Autor

* **Andrés E. Restrepo**

---

package com.atlanticsoft.automaticsendemail.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;

import javax.mail.MessagingException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.atlanticsoft.automaticsendemail.email.SendEmail;
import com.atlanticsoft.automaticsendemail.response.Response;

public class LambdaService {
	
	Response response = new Response();
	
	public void extractQueueBody(InputStream input) {
		// Convert the entry to json and extract the records from the queue event
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		JSONParser parser = new JSONParser();
		JSONObject event = null;
		JSONArray records = null;
		JSONObject queueBody;
		try {
			event = (JSONObject) parser.parse(reader);
			records = (JSONArray) parser.parse(event.get("Records").toString());
			// Send the JSON
			for (Object object : records) {
				queueBody = (JSONObject) parser.parse(object.toString());
				sendEmail((queueBody.get("body")).toString());
			}
		} catch (Exception e) {
			setResponse("Error", e.getMessage());
		}
	}
	
	private void sendEmail(String tagEmail) throws MessagingException,  IOException, ParseException {
		/*
		CONFIGSET	
		FROM	cardpoolusasupply@gmail.com
		HOST	email-smtp.us-west-2.amazonaws.com
		PORT	587
		SMTP_PASSWORD	BIXsOJcyM0NbOQneRTNg8aYAlP8HFC04C7r4NEvFVVew
		SMTP_USERNAME	AKIAQAFOCJ4JAIRCKNUC
		*/
		String SMTP_USERNAME = System.getenv("SMTP_USERNAME");
		String SMTP_PASSWORD = System.getenv("SMTP_PASSWORD");
		String CONFIGSET = System.getenv("SMTP_PASSWORD");
		String HOST = System.getenv("HOST");
		String PORT = System.getenv("PORT");
		String FROM = System.getenv("FROM");
		
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = null;
		JSONObject emailJson;
		try {
			jsonObject = (JSONObject) parser.parse(tagEmail);
			emailJson = (JSONObject) (jsonObject.get("email"));
			SendEmail sendEmail = new SendEmail();
			setResponse(emailJson.get("to").toString(),
					sendEmail.sendEmail(
							SMTP_USERNAME,
							SMTP_PASSWORD, 
							CONFIGSET,
							HOST,
							PORT,
							FROM,
							emailJson.get("to").toString(),
							emailJson.get("fromName").toString(),
							emailJson.get("subject").toString(),
							emailJson.get("emailBody").toString()
							)
					);
		} catch (org.json.simple.parser.ParseException e) {
			setResponse("Error", e.getMessage());
		}/**/
	}
	
	public void setResponse(String key, String value) {
		response.setResponse(key, value);
	}

	public JSONObject getResponse() {
		return response.getResponse();
	}

}

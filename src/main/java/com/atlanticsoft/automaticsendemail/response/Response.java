package com.atlanticsoft.automaticsendemail.response;

import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class Response {
	
	private JSONObject body = new JSONObject();
	
	public Response() {
	}
	
	public void setResponse(String key, String value) {
		if(value == "") {
			this.body.put(key, this.body);
		} else {
			body.put(key, value);
		}
	}
	
	public JSONObject getResponse() {
		String headers = "Automatic Send Email Function";
		int statusCode = 100;
		
		JSONObject headerJson = new JSONObject();
		JSONObject responseJson = new JSONObject();
		headerJson.put("header", headers);
		responseJson.put("headers", headerJson);
		responseJson.put("statusCode", statusCode);
		responseJson.put("body", this.body);

		return responseJson;
	}
}
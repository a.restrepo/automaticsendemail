package com.atlanticsoft.automaticsendemail.controller;

import com.atlanticsoft.automaticsendemail.service.LambdaService;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

@SuppressWarnings("unchecked")
public class LambdaController implements RequestStreamHandler {
	
	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		LambdaService lambdaService = new LambdaService();
		lambdaService.extractQueueBody(input);
		OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
		writer.write(lambdaService.getResponse().toString());
		writer.close();
	}
	
}
package com.atlanticsoft.automaticsendemail.email;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@SuppressWarnings("unchecked")
public class SendEmail {

	public String sendEmail(String SMTP_USERNAME, String SMTP_PASSWORD, String CONFIGSET, String HOST, String PORT,
			String FROM, String TO, String FROMNAME, String SUBJECT, String BODY)
			throws UnsupportedEncodingException, MessagingException {
		
		try {
			// Create a Properties object to contain connection configuration information.
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.port", PORT);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");
			// Create a Session object to represent a mail session with the specified
			// properties.
			Session session = Session.getDefaultInstance(props);
			// Create a message with the specified information.
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(FROM, FROMNAME));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
			msg.setSubject(SUBJECT);
			msg.setContent(BODY, "text/html");
			// Add a configuration set header. Comment or delete the next line if you are
			// not using a configuration set
			// msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
			// Create a transport.
			Transport transport = session.getTransport();
			// Send the message.
			try {
				// Connect to Amazon SES using the SMTP username and password you specified
				// above.
				transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
				// Send the email.
				transport.sendMessage(msg, msg.getAllRecipients());
				return "OK";
			} catch (Exception e) {
				return e.getMessage();
			} finally {
				// Close and terminate the connection.
				transport.close();
			}

		} catch (Exception e) {
			return e.getMessage();
		}
	}

}